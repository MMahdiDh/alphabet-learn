package mMahdiDh.oyna.ui.adaptor

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import mMahdiDh.oyna.databinding.ItemMenuBinding
import mMahdiDh.oyna.utli.model.MenuItem
import mMahdiDh.oyna.utli.model.MyDiffUtil

class MenuListAdaptor(val func : (String) -> Unit):
    ListAdapter<MenuItem, MenuListAdaptor.MyViewHolder>(MyDiffUtil<MenuItem> { oldItem, newItem ->
    oldItem.name == newItem.name }) {

    var funcFlag = true
    var currentCategory = ""

    class MyViewHolder(private val _binding: ItemMenuBinding, val func : (String) -> Unit):
        RecyclerView.ViewHolder(_binding.root) {

        fun binding(dataset: MenuItem, currentCategory:String): String {
            var result = currentCategory
            _binding.apply {
//                imgMenu.setImageURI()
                txtMenu.text = dataset.name
                cnstrntMenu.setOnClickListener {
                    if (dataset.name != currentCategory) {
                        this@MyViewHolder.func(dataset.name)
                        result = dataset.name
                    }
                }
            }
            return result
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
        ItemMenuBinding.inflate(LayoutInflater.from(parent.context)),
        this@MenuListAdaptor.func
    )


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val dataset = getItem(position)
        if ( funcFlag and (position == 0)) {
            this@MenuListAdaptor.func(dataset.name)
        }
        currentCategory = holder.binding(dataset, currentCategory)
    }

}