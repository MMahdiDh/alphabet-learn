package mMahdiDh.oyna.ui.adaptor

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import mMahdiDh.oyna.databinding.ItemTrainingGroundAnswerBinding
import mMahdiDh.oyna.utli.model.Answer
import mMahdiDh.oyna.utli.model.MyDiffUtil

class AnswersListAdapter: ListAdapter<Answer, AnswersListAdapter.MyViewHolder>(MyDiffUtil<Answer> { oldItem, newItem -> oldItem.url == newItem.url }) {
    class MyViewHolder(private val binding: ItemTrainingGroundAnswerBinding): RecyclerView.ViewHolder(binding.root) {
        fun binding(dataset:Answer) {
            binding.apply {
//                imgAnswer.setImageURI()
                imgAnswer.setOnClickListener {
                    if (dataset.isCorrect) {
                        // TODO
                    } else {
                        // TODO
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
        ItemTrainingGroundAnswerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding(getItem(position))
    }
}