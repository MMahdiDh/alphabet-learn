package mMahdiDh.oyna.ui.adaptor

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import mMahdiDh.oyna.databinding.ItemTrainingGroundQuestionBinding
import mMahdiDh.oyna.utli.model.MyDiffUtil

class QuestionslistAdapter: ListAdapter<String, QuestionslistAdapter.MyViewHolder> (MyDiffUtil<String> { oldItem, newItem -> oldItem == newItem }){
    private var selectedPosition: Int = -1
    private var selected: String = ""

    class MyViewHolder(private val binding: ItemTrainingGroundQuestionBinding): RecyclerView.ViewHolder(binding.root) {
        fun binding(dataset:String, selected: String): String {
            var flag = false
            binding.apply {
                chpQuestion.text = dataset
                chpQuestion.setOnCloseIconClickListener {
                    if (dataset != selected) {
//                        chpQuestion.isSelected = true
                        flag = true
                    }
                }
                return@binding if (flag) (chpQuestion.text).toString() else dataset
            }
        }
        fun unselect(dataset:String) {
//            binding.chpQuestion.isCheckable = false
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
        ItemTrainingGroundQuestionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val result:String = holder.binding(getItem(position), "")
        if(result != selected) {
            holder.unselect(getItem(selectedPosition))
            selected = result
            selectedPosition = position
        }
    }
}