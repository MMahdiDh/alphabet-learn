package mMahdiDh.oyna.ui.adaptor

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import mMahdiDh.oyna.databinding.ItemLevelBinding
import mMahdiDh.oyna.utli.model.Level
import mMahdiDh.oyna.utli.model.MyDiffUtil

class LevelListAdaptor:
    ListAdapter<Level, LevelListAdaptor.MyViewHolder>(MyDiffUtil<Level>{oldItem, newItem ->
        oldItem.id == newItem.id
    }) {
        class MyViewHolder(private val _binding: ItemLevelBinding): RecyclerView.ViewHolder(_binding.root){
            fun binding(dataset:Level) {
                _binding.apply {
                    txtLevel.text = dataset.name
//                    imgLevel.setImageURI()
                    crdLevel.setOnClickListener {
                        Log.e("MMahdi", "${dataset.name}")
                    }
                }
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(
        ItemLevelBinding.inflate(LayoutInflater.from(parent.context))
    )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding(getItem(position))
    }
}