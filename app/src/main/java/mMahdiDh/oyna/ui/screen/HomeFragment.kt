package mMahdiDh.oyna.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import mMahdiDh.oyna.R
import mMahdiDh.oyna.databinding.FragmentHomeBinding
import mMahdiDh.oyna.ui.adaptor.LevelListAdaptor
import mMahdiDh.oyna.ui.adaptor.MenuListAdaptor

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        onCreatRecyclerView(
            MenuListAdaptor { name -> viewModel.getLevels(name) },
            LevelListAdaptor()
        )
    }

    fun onCreatRecyclerView(menuListAdaptor:MenuListAdaptor, levelListAdaptor: LevelListAdaptor) {

        binding.apply {
            rclrMenu.apply {
                adapter = menuListAdaptor
                layoutManager = LinearLayoutManager(activity)
            }
            rclrLevels.apply {
                adapter = levelListAdaptor
                layoutManager = LinearLayoutManager(activity)
            }

            viewModel.menuItem.observe(viewLifecycleOwner, Observer { dataset ->
                menuListAdaptor.submitList(dataset)
            })
            viewModel.getMenuItem()

            viewModel.levelsList.observe(viewLifecycleOwner, Observer { dataset ->
                levelListAdaptor.submitList(dataset)
            })
        }
    }

}