package mMahdiDh.oyna.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mMahdiDh.oyna.repository.repository
import mMahdiDh.oyna.utli.model.Answer

class TrainingGroundViewModel : ViewModel() {
    private val _questions = MutableLiveData<List<String>>()
    val question: LiveData<List<String>>
        get() = _questions

    private val _answers = MutableLiveData<List<Answer>>()
    val answers: LiveData<List<Answer>>
        get()  = _answers

    fun prepareTrain() = viewModelScope.launch {
        val resualt = repository.propareTrain()
        _questions.value = resualt[0] as List<String>
        _answers.value = resualt[1] as List<Answer>
    }
}