package mMahdiDh.oyna.ui.screen

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mMahdiDh.oyna.repository.repository
import mMahdiDh.oyna.utli.model.Level
import mMahdiDh.oyna.utli.model.MenuItem

class HomeViewModel : ViewModel() {

    private val _menuList = MutableLiveData<List<MenuItem>>()
    val menuItem: LiveData<List<MenuItem>>
        get() = _menuList

    private val _levelsList = MutableLiveData<List<Level>>()
    val levelsList: LiveData<List<Level>>
        get() = _levelsList

    fun getMenuItem() = viewModelScope.launch {
         _menuList.value = repository.getMenuItem()
     }

    fun getLevels(gameName: String) = viewModelScope.launch {
        _levelsList.value = repository.getLevels(gameName)
    }


}