package mMahdiDh.oyna.ui.screen

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import mMahdiDh.oyna.databinding.FragmentTrainingGroundBinding
import mMahdiDh.oyna.ui.adaptor.AnswersListAdapter
import mMahdiDh.oyna.ui.adaptor.QuestionslistAdapter

class TrainingGroundFragment : Fragment() {

    private lateinit var viewModel: TrainingGroundViewModel
    private lateinit var binding: FragmentTrainingGroundBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrainingGroundBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TrainingGroundViewModel::class.java)

        onCreateRecyclerView(QuestionslistAdapter(), AnswersListAdapter())
    }

    fun onCreateRecyclerView(questionslistAdapter: QuestionslistAdapter, answersListAdapter: AnswersListAdapter) {
        binding.apply {
            rclrQuestions.apply {
                adapter = questionslistAdapter
                layoutManager = LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL, false)
            }
            rclrAnswers.apply {
                adapter = answersListAdapter
                layoutManager = GridLayoutManager(this.context, 2)
            }
        }
        viewModel.question.observe(viewLifecycleOwner, Observer { dataset ->
            questionslistAdapter.submitList(dataset)
        })
        viewModel.answers.observe(viewLifecycleOwner, Observer { dataset ->
            answersListAdapter.submitList(dataset)
        })
    }

}