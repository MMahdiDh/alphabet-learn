package mMahdiDh.oyna.utli.model

data class Level(
    val id: Int,
    val name: String,
    val image: String,
)