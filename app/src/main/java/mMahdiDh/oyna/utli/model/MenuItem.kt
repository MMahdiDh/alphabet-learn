package mMahdiDh.oyna.utli.model

data class MenuItem (
    val name: String,
    val imageUrl: String
)