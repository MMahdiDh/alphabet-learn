package mMahdiDh.oyna.utli.model

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

class MyDiffUtil<T>(
    val areItemsTheSameLamda: (T,T) -> Boolean
): DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
        areItemsTheSameLamda(oldItem, newItem)

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
        oldItem == newItem
}