package mMahdiDh.oyna.utli.model

data class Answer(
    val url: String,
    val isCorrect: Boolean
)
