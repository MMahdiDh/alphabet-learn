package mMahdiDh.oyna.repository

import mMahdiDh.oyna.utli.model.Answer
import mMahdiDh.oyna.utli.model.Level
import mMahdiDh.oyna.utli.model.MenuItem

object repository {

    suspend fun getMenuItem() = listOf(
        MenuItem("Alphabet", "")
    )

    suspend fun getLevels(gameName: String) = listOf(
        Level(0, "Aa", ""),
        Level(1, "Aa", ""),
        Level(2, "Aa", ""),
        Level(3, "Aa", ""),
        Level(4, "Aa", ""),
        Level(5, "Aa", ""),
        Level(6, "Aa", ""),
        Level(7, "Aa", ""),
        Level(8, "Aa", ""),
        Level(9, "Aa", ""),
        Level(10, "Aa", "")
    )

    fun propareTrain() = listOf(
        listOf(
            "a", "b" , "c"
        ),
        listOf(
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false),
            Answer("",false)
        )
    )
}